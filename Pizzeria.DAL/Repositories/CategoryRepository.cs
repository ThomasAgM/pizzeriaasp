﻿using Pizzeria.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolBox.ADO.Utils;

namespace Pizzeria.DAL.Repositories
{
    public class CategoryRepository : BaseRepository<Category>
    {
        public CategoryRepository(string connectionString, string providerName) : base(connectionString, providerName)
        {
        }
    }
}
