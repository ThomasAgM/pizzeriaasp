﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pizzeria.ASP.Models
{
    public class RegisterModel
    {
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string Email { get; set; }
        public string PlainPassword { get; set; }
        public string ConfirmPassword { get; set; }

    }
}