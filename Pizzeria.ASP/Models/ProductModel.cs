﻿using Pizzeria.ASP.DI;
using Pizzeria.DAL.Entities;
using Pizzeria.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Pizzeria.ASP.Models
{
    public class ProductModel
    {
        public int Id { get; set; }


        [Required]
        [MaxLength(50)]
        [MinLength(2)]
        public string Name { get; set; }


        [Required]
        [MaxLength(10000)]
        [MinLength(2)]
        public string Description { get; set; }

        [MaxLength(255)]
        public string Image { get; set; }


        [Required]
        [Range(0,1000)]
        public decimal Price { get; set; }
        public int? CategoryId { get; set; }
        public string CategoryName { get; set; }

        private IEnumerable<CategoryModel> _ListCategory;


        //Lazy loading = > tu charges que quand tu as besoin. 
        public IEnumerable<CategoryModel> ListCategory { get 
            {
                if (_ListCategory != null) return _ListCategory;


                CategoryRepository repo = ServicesLocator.GetService<CategoryRepository>();

                IEnumerable<Category> r = repo.Get();
                return _ListCategory = r.Select(c => new CategoryModel
                {
                    Name = c.Name,
                    Id = c.Id
                });
            } }

        public HttpPostedFileBase File { get; set; }



        private IEnumerable<ProductModel> _listProduct;

        public IEnumerable<ProductModel> ListProduct
        {
            get
            {
                if (_listProduct != null) return _listProduct;


                ProductRepository repo = ServicesLocator.GetService<ProductRepository>();

                IEnumerable<Product> r = repo.Get();
                return _listProduct = r.Select(c => new ProductModel
                {
                    Id = c.Id,
                    Name = c.Name,
                    Description = c.Description,
                    Price = c.Price,
                    Image = c.Image
                });
            }
        }
    }
}