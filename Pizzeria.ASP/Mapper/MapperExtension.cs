﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace Pizzeria.ASP.Mapper
{
    public static class MapperExtension
    {
        public static T MapTo<T>(this object from)
            where T : new()
        {
            T result = new T();

            foreach (var p in typeof(T).GetProperties())
            {
                if(from.GetType().GetProperty(p.Name) != null)
                {
                    PropertyInfo prop
                        = from.GetType().GetProperty(p.Name);
                    object value = prop.GetValue(from);
                    p.SetValue(result, value);
                }
            }
            return result;
        }
    }
}