﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Web.Mvc;

namespace Pizzeria.ASP.Filters
{
    public class RoleAttribute : AuthorizeAttribute
    {
        private string[] _role;

        public RoleAttribute(params string[] role)
        {
            _role = role;
        }

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if(!_role.Contains(HttpContext.Current.Session["Role"] as string))
            {
                throw new HttpException();
            }
        }
    }
}