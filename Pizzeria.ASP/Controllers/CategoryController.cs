﻿using Pizzeria.ASP.DI;
using Pizzeria.ASP.Mapper;
using Pizzeria.ASP.Models;
using Pizzeria.DAL.Entities;
using Pizzeria.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pizzeria.ASP.Controllers
{
    public class CategoryController : Controller
    {
        // GET: Category
        public ActionResult Index()
        {
            ////Récupérer les categories dans la db
            //CategoryRepository repo = new CategoryRepository(
            //    @"Server =DESKTOP-G9D6CA1\SQLSERVER; Initial Catalog=pizzeria; UID =sa; PWD=Luffyzorosanji12",
            //    "System.Data.SqlClient"
            //    );

            //CategoryRepository repo = new CategoryRepository(
            //  @"Server =AGM122\SQLSERVER; Initial Catalog=pizzeria; UID =sa; PWD=Luffyzorosanji12",
            //  "System.Data.SqlClient"
            //  );

            CategoryRepository repo = ServicesLocator.GetService<CategoryRepository>();

            IEnumerable<Category> categories = repo.Get();
            IEnumerable<CategoryModel> model = categories.Select(x => x.MapTo<CategoryModel>());

            return View(model);
        }

        [HttpGet]
        public ActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Add(CategoryModel model)
        {
            if (ModelState.IsValid)
            {
                //recupérèr le repo
                //enregistrer les données en db

                CategoryRepository repo = ServicesLocator.GetService<CategoryRepository>();


                Category c = model.MapTo<Category>();

                repo.Insert(c);
                return RedirectToAction("Index");
            }
            return View(model);
        }

        public ActionResult Delete(int id)
        {
            CategoryRepository repo = ServicesLocator.GetService<CategoryRepository>();

            repo.Delete(id);
            return RedirectToAction("Index");
        }
    }
}