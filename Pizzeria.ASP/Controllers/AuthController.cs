﻿using Pizzeria.ASP.DI;
using Pizzeria.ASP.Mapper;
using Pizzeria.ASP.Models;
using Pizzeria.DAL.Entities;
using Pizzeria.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Security.Cryptography;
using System.Text;
using Pizzeria.ASP.Services;

namespace Pizzeria.ASP.Controllers
{
    public class AuthController : Controller
    {
        // GET: Auth
        [HttpGet]
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Register(RegisterModel model)
        {

            if (ModelState.IsValid)
            {
                UserRepository repo = ServicesLocator.GetService<UserRepository>();

                User u = model.MapTo<User>();

                byte[] encoded = ServicesLocator.GetService<HashService>()
                     .HashPassword(model.PlainPassword);

                u.Password = encoded;

                u.Role = "Customer";

                repo.Insert(u);
                return RedirectToAction("Login");
            }
            return View();
        }

        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }


        [HttpPost]
        public ActionResult Login(LoginModel model)
        {

            if (ModelState.IsValid)
            {
                UserRepository repo = ServicesLocator.GetService<UserRepository>();

                User u = repo.Get().SingleOrDefault(x => x.Email == model.Email);

                if(u == null)
                {
                    ViewBag.Error = "Bad Credentials";
                    return View(model);
                }



                byte[] encoded = ServicesLocator.GetService<HashService>()
                    .HashPassword(model.PlainPassword);
                //Nous ne pouvons pas comparer des tableaux de byte (jamais la même réfèrence) on doit d'abord les convertir en string.
                if (Encoding.UTF8.GetString(u.Password) 
                    == Encoding.UTF8.GetString(encoded))
                {
                    //enregistrer l'user dans une session
                    Session["Role"] = u.Role;
                    Session["Email"] = u.Email;
                    Session["Id"] = u.Id;
                    //rediriger
                    return RedirectToAction("Index", "Product");
                }
                ViewBag.Error = "Bad Credentials";
                return View(model);

            }
            return View(model);
        }
    }
}