﻿using Pizzeria.ASP.DI;
using Pizzeria.ASP.Filters;
using Pizzeria.ASP.Mapper;
using Pizzeria.ASP.Models;
using Pizzeria.DAL.Entities;
using Pizzeria.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;

namespace Pizzeria.ASP.Controllers
{
    public class ProductController : Controller
    {
        // GET: Product
        [Role("Customer","Admin")]
        public ActionResult Index()
        {

            ////Récupérer les categories dans la db
            //ProductRepository repo = new ProductRepository(
            //    @"Server =DESKTOP-G9D6CA1\SQLSERVER; Initial Catalog=pizzeria; UID =sa; PWD=Luffyzorosanji12",
            //    "System.Data.SqlClient"
            //    );

            ProductRepository repo = ServicesLocator.GetService<ProductRepository>();


            IEnumerable<Product> Products = repo.Get();
            IEnumerable<ProductModel> model = Products
                .Select(x => x.MapTo<ProductModel>());

            return View(model);
        }

        [HttpGet] //  <= Implicite, pas obligatoire
        [Role("Admin")]
        public ActionResult Add()
        {
            return View(new ProductModel());
        }

        [HttpPost]
        [Role("Admin")]

        public ActionResult Add(ProductModel model)
        {
            if (ModelState.IsValid)
            {
                if(model.File != null)
                {
                    string fileName = Guid.NewGuid().ToString();
                    model.File.SaveAs(HostingEnvironment.MapPath("~/Content/Images/Products/") + fileName + model.File.FileName);

                    model.Image = "/Content/Images/Products/" + fileName + model.File.FileName;
                }

                ProductRepository repo = ServicesLocator.GetService<ProductRepository>();

                Product p = model.MapTo<Product>();

                repo.Insert(p);
                return RedirectToAction("Index");
            }
            return View(model);
        }

        [Role("Admin")]

        public ActionResult Select()
        {
            return View(new ProductModel());
        }

        public ActionResult Update(int id)
        {
            Product x = ServicesLocator.GetService<ProductRepository>().GetById(id);
            ProductModel model = x.MapTo<ProductModel>();

            //new ProductModel
            //{
            //    Id = x.Id,
            //    Name = x.Name,
            //    CategoryId = x.CategoryId,
            //    Description = x.Description,
            //    Image = x.Image,
            //    Price = x.Price
            //};
            return View(model);
        }

        [HttpPost]
        [Role("Admin")]

        public ActionResult Update(ProductModel model)
        {
            if (ModelState.IsValid)
            {

                if (model.File != null)
                {
                    if(model.Image != null)
                    {
                        System.IO.File.Delete(HostingEnvironment.MapPath(model.Image));
                    }
                    string fileName = Guid.NewGuid().ToString();
                    model.File.SaveAs(HostingEnvironment.MapPath("~/Content/Images/Products/") + fileName + model.File.FileName);

                    model.Image = "/Content/Images/Products/" + fileName + model.File.FileName;
                }

                ProductRepository repo = ServicesLocator.GetService<ProductRepository>();


                Product c = model.MapTo<Product>();

                //new Product
                //{
                //    Id = model.Id,
                //    Name = model.Name,
                //    Description = model.Description,
                //    Price = model.Price,
                //    Image = model.Image,
                //    CategoryId = model.CategoryId
                //};

                repo.Update(c);
                return RedirectToAction("Index");
            }
            return View(model);
        }
    }
}