﻿using Pizzeria.ASP.DI;
using Pizzeria.DAL.Entities;
using Pizzeria.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Pizzeria.ASP.Validators
{
    public class NameCatValidator : ValidationAttribute
    {
        public NameCatValidator()
        {
            ErrorMessage = "Le nom de cette catégorie existe déjà";
        }
        public override bool IsValid(object value)
        {

            CategoryRepository repo = ServicesLocator.GetService<CategoryRepository>();


            if (value == null) return true;

            IEnumerable<Category> liste = repo.Get();

            bool ok = true;
            foreach (var item in liste)
            {
                if (item.Name == value.ToString())
                {
                    ok = false;
                }
            }

            if (ok)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    }
}